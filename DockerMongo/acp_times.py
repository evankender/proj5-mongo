"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow
import math
#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    days = 0
    brevet_start_time = arrow.get(brevet_start_time)
    open_time_table = { 200:brevet_start_time.shift(hours=+5, minutes=+53),
                        300:brevet_start_time.shift(hours=+9),
                        400:brevet_start_time.shift(hours=+12, minutes=+8),
                        600:brevet_start_time.shift(hours=+18, minutes=+48),
                        1000:brevet_start_time.shift(days=+1, hours=+9, minutes=+5)
                        }

    if control_dist_km == 0:
        return brevet_start_time.isoformat()

    elif control_dist_km >= brevet_dist_km:
        for key in open_time_table:
            if key == brevet_dist_km:
                return open_time_table[key].isoformat()
    else:
        days = 0
        hours = 0
        minutes = 0
        seconds = 0
        speed = 34
        while control_dist_km >= 200:
            time = 200/speed
            hours += int(time)
            minutes += int((time*60) % 60)
            seconds += (time*3600) % 60

            if speed > 28:
                speed -= 2

            control_dist_km = control_dist_km - 200

        if control_dist_km > 0:
            time = control_dist_km/speed

            hours += int(time)
            minutes += int((time*60) % 60)
            seconds += (time*3600) % 60

        while seconds > 59:
            minutes += 1
            seconds -= 60

        while minutes > 59:
            hours += 1
            minutes -= 60

        while hours > 23:
            days += 1
            hours -= 24

        if seconds > 29:
            minutes += 1
            seconds = 0
        else:
            seconds = 0

        return brevet_start_time.shift(days=+days,hours=+int(hours), minutes=+int(minutes),seconds=+int(seconds)).isoformat()

def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    days = 0
    brevet_start_time = arrow.get(brevet_start_time)
    close_time_table = { 200:brevet_start_time.shift(hours=+13, minutes=+30),
                         300:brevet_start_time.shift(hours=+20),
                         400:brevet_start_time.shift(days=+1,hours=+3),
                         600:brevet_start_time.shift(days=+1,hours=+16),
                         1000:brevet_start_time.shift(days=+3,hours=+3)
    }
    if control_dist_km == 0:
        return brevet_start_time.shift(hours=+1).isoformat()

    elif control_dist_km <= 60:
        hours = 0
        minutes = 0
        seconds = 0
        time = control_dist_km/20
        hours += int(time)
        minutes += int((time*60) % 60)
        seconds += (time*3600) % 60

        while minutes > 59:
            hours += 1
            minutes -= 60

        while hours > 23:
            days += 1
            hours -= 24

        if seconds > 29:
            minutes += 1
            seconds = 0
        else:
            seconds = 0

        hours += 1
        return brevet_start_time.shift(days=+days,hours=+int(hours), minutes=+int(minutes), seconds=+int(seconds)).isoformat()

    elif control_dist_km >= brevet_dist_km:
        for key in close_time_table:
            if key == brevet_dist_km:
                return close_time_table[key].isoformat()

    else:
        hours = 0
        minutes = 0
        seconds = 0
        days = 0
        speed = 15
        while control_dist_km >= 600:
            time = 600/speed
            hours += int(time)
            minutes += int((time*60) % 60)
            seconds += (time*3600) % 60
            speed = 11.428
            control_dist_km = control_dist_km - 600

        if control_dist_km > 0:
            time = control_dist_km/speed
            hours += int(time)
            minutes += int((time*60) % 60)
            seconds += (time*3600) % 60

        while minutes > 59:
            hours += 1
            minutes -= 60

        while hours > 23:
            days += 1
            hours -= 24

        if seconds > 29:
            minutes += 1
            seconds = 0
        else:
            seconds = 0

        return brevet_start_time.shift(days=+days,hours=+int(hours), minutes=+int(minutes), seconds=+int(seconds)).isoformat()
