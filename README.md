# Project 4: Brevet time calculator with Ajax

This is a reimplimation of the RUSA ACP controle time calculator with flask, ajax, and mongodb.

Credits to Michal Young for the initial version of this code.

Can calculate brevits up to 1000km and with a maxium of 20 control points.

This calculator does implement the french algorithm for control points less then 60km

# Use

To use this calculator first select your brevet distance, start date, and start time. 

Although it will update the controls open close times if you want to change any of these values later.

After this you are free to enter your control distances in either km or miles, however the total distance is always in km.

Once you have filled in the control points there is a submit button which will add all of your control points, in km, along with its open and close time.

Finally, once you have submitted there is a display button which will display your most recent submission in a tab.


# Notes

If you enter a control point it must be three things:

	
	-A non negative number(floats are fine)
	-No longer than 20% the total length of the brevit
	-Bigger than the previous control point

For submission and displaying your brevet: 

	
	-To submit, there must be atleast one valid control point
	-To display, you need a submission who's last control point is valid and alteast equal to the total brevet length
	

# Testing

**Frontend/Backend**

First test: Load the page and press the "Submit" button before entering any value or entering an invalid one(negative or greater than total length)

	This will display and alert box with "Enter a control point before submitting"
	
Second test: Load the page and press the "Display brevet" button without pressing the submit button

	This will display and alert box with "Submit your brevit before trying to load"
	
Third test: Load the page and type in a control point less than the brevets total length then press the "Submit" button" followed by the "Display brevet" button

	This will display and alert box with "The last control point ('last control point'km) must be atleast 'brevet total distance'km"


**Algorithm**


To run the test file you must have an open docker container and be in the brevet folder then call:

	docker exec -it <container_id> nosetests
	

# Developers 

There is a test file include incase you want to modify the code and test the new implementation.
If you want to increase the brevet length there is a table that handles cases when a control point is over the brevet length but not more than 20%. This would be a good place to start.
If you want to be able to display a brevet with its last control point less than the total length get rid of the if statement on starting on line ~280 of calc.html.

# How times are calculated 

If interested in how times are calculated.
See this link: https://rusa.org/pages/acp-brevet-control-times-calculator

# Author

Evan Kender

ekender@uoregon.edu

base code author: Michal Young

# Requirements

arrow,
Flask,
nose,
pep8,
autopep8,
and pymongo



