import nose
import acp_times
import arrow

def test_open_time_200():
    var0 = acp_times.open_time(0, 200, "2020-11-10T00:00:00+00:00")
    var60 = acp_times.open_time(60, 200, "2020-11-10T00:00:00+00:00")
    var125 = acp_times.open_time(125, 200, "2020-11-10T00:00:00+00:00")
    var133 = acp_times.open_time(133, 200, "2020-11-10T00:00:00+00:00")
    var140 = acp_times.open_time(140, 200, "2020-11-10T00:00:00+00:00")
    var180 = acp_times.open_time(180, 200, "2020-11-10T00:00:00+00:00")
    var205 = acp_times.open_time(205, 200, "2020-11-10T00:00:00+00:00")

    nose.tools.eq_(arrow.get(var0), arrow.get("2020-11-10T00:00:00+00:00"))
    nose.tools.eq_(arrow.get(var60), arrow.get("2020-11-10T01:46:00+00:00"))
    nose.tools.eq_(arrow.get(var125), arrow.get("2020-11-10T03:41:00+00:00"))
    nose.tools.eq_(arrow.get(var133), arrow.get("2020-11-10T03:55:00+00:00"))
    nose.tools.eq_(arrow.get(var140), arrow.get("2020-11-10T04:07:00+00:00"))
    nose.tools.eq_(arrow.get(var180), arrow.get("2020-11-10T05:18:00+00:00"))
    nose.tools.eq_(arrow.get(var205), arrow.get("2020-11-10T05:53:00+00:00"))

def test_open_time_400():
    var0 = acp_times.open_time(0, 400, "2020-11-10T00:00:00+00:00")
    var180 = acp_times.open_time(180, 400, "2020-11-10T00:00:00+00:00")
    var205 = acp_times.open_time(205, 400, "2020-11-10T00:00:00+00:00")
    var337 = acp_times.open_time(337, 400, "2020-11-10T00:00:00+00:00")
    var338 = acp_times.open_time(338, 400, "2020-11-10T00:00:00+00:00")
    var399 = acp_times.open_time(399, 400, "2020-11-10T00:00:00+00:00")
    var405 = acp_times.open_time(405, 400, "2020-11-10T00:00:00+00:00")

    nose.tools.eq_(arrow.get(var0), arrow.get("2020-11-10T00:00:00+00:00"))
    nose.tools.eq_(arrow.get(var180), arrow.get("2020-11-10T05:18:00+00:00"))
    nose.tools.eq_(arrow.get(var205), arrow.get("2020-11-10T06:02:00+00:00"))
    nose.tools.eq_(arrow.get(var337), arrow.get("2020-11-10T10:10:00+00:00"))
    nose.tools.eq_(arrow.get(var338), arrow.get("2020-11-10T10:12:00+00:00"))
    nose.tools.eq_(arrow.get(var399), arrow.get("2020-11-10T12:06:00+00:00"))
    nose.tools.eq_(arrow.get(var405), arrow.get("2020-11-10T12:08:00+00:00"))

def test_close_time_400():
    var0 = acp_times.close_time(0, 400, "2020-11-10T00:00:00+00:00")
    var180 = acp_times.close_time(180, 400, "2020-11-10T00:00:00+00:00")
    var205 = acp_times.close_time(205, 400, "2020-11-10T00:00:00+00:00")
    var337 = acp_times.close_time(337, 400, "2020-11-10T00:00:00+00:00")
    var338 = acp_times.close_time(338, 400, "2020-11-10T00:00:00+00:00")
    var399 = acp_times.close_time(399, 400, "2020-11-10T00:00:00+00:00")
    var405 = acp_times.close_time(405, 400, "2020-11-10T00:00:00+00:00")

    nose.tools.eq_(arrow.get(var0), arrow.get("2020-11-10T01:00:00+00:00"))
    nose.tools.eq_(arrow.get(var180), arrow.get("2020-11-10T12:00:00+00:00"))
    nose.tools.eq_(arrow.get(var205), arrow.get("2020-11-10T13:40:00+00:00"))
    nose.tools.eq_(arrow.get(var337), arrow.get("2020-11-10T22:28:00+00:00"))
    nose.tools.eq_(arrow.get(var338), arrow.get("2020-11-10T22:32:00+00:00"))
    nose.tools.eq_(arrow.get(var399), arrow.get("2020-11-11T02:36:00+00:00"))
    nose.tools.eq_(arrow.get(var405), arrow.get("2020-11-11T03:00:00+00:00"))

def test_close_time_600_1000():
    var0 = acp_times.close_time(0, 1000, "2020-11-10T00:00:00+00:00")
    var485 = acp_times.close_time(485, 1000, "2020-11-10T00:00:00+00:00")
    var600 = acp_times.close_time(600, 1000, "2020-11-10T00:00:00+00:00")
    var601 = acp_times.close_time(601, 1000, "2020-11-10T00:00:00+00:00")
    var605 = acp_times.close_time(605, 1000, "2020-11-10T00:00:00+00:00")
    var850 = acp_times.close_time(850, 1000, "2020-11-10T00:00:00+00:00")
    var1005 = acp_times.close_time(1005, 1000, "2020-11-10T00:00:00+00:00")

    nose.tools.eq_(arrow.get(var0), arrow.get("2020-11-10T01:00:00+00:00"))
    nose.tools.eq_(arrow.get(var485), arrow.get("2020-11-11T08:20:00+00:00"))
    nose.tools.eq_(arrow.get(var600), arrow.get("2020-11-11T16:00:00+00:00"))
    nose.tools.eq_(arrow.get(var601), arrow.get("2020-11-11T16:05:00+00:00"))
    nose.tools.eq_(arrow.get(var605), arrow.get("2020-11-11T16:26:00+00:00"))
    nose.tools.eq_(arrow.get(var850), arrow.get("2020-11-12T13:53:00+00:00"))
    nose.tools.eq_(arrow.get(var1005), arrow.get("2020-11-13T03:00:00+00:00"))

def test_start_time_():
    var0open = acp_times.open_time(0, 1000, "2020-11-20T10:00:00+00:00")
    var0close = acp_times.close_time(0, 1000, "2020-11-20T10:00:00+00:00")

    nose.tools.eq_(arrow.get(var0open), arrow.get("2020-11-20T10:00:00+00:00"))
    nose.tools.eq_(arrow.get(var0close), arrow.get("2020-11-20T11:00:00+00:00"))

def test_special():
    var50open = acp_times.open_time(50, 1000, "2020-11-10T00:00:00+00:00")
    var50close = acp_times.close_time(50, 1000, "2020-11-10T00:00:00+00:00")
    var900open = acp_times.open_time(900, 1000, "2020-11-10T00:00:00+00:00")
    var900close = acp_times.close_time(900, 1000, "2020-11-10T00:00:00+00:00")
    var205 = acp_times.close_time(205, 200, "2020-11-10T00:00:00+00:00")

    nose.tools.eq_(arrow.get(var50open), arrow.get("2020-11-10T01:28:00+00:00"))
    nose.tools.eq_(arrow.get(var50close), arrow.get("2020-11-10T03:30:00+00:00"))
    nose.tools.eq_(arrow.get(var900open), arrow.get("2020-11-11T05:31:00+00:00"))
    nose.tools.eq_(arrow.get(var900close), arrow.get("2020-11-12T18:15:00+00:00"))
    nose.tools.eq_(arrow.get(var205), arrow.get("2020-11-10T13:30:00+00:00"))
